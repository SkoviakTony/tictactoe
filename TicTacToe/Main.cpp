// Tic Tac Toe
// Use this main.cpp to test your TicTacToe class

//I used a two-dimensional array to practice looping through them.
//I also asked the user for two inputs instead of one, which is a bit clunky. Sorry about that.

#include <iostream>
#include <conio.h>

#include "TicTacToe.h"

using namespace std;

int main()
{
	TicTacToe* pGame = nullptr;

	while (true)
	{
		if (pGame) delete pGame;
		pGame = new TicTacToe;

		// play the game
		while (!pGame->IsOver())
		{
			pGame->DisplayBoard();

			int x_coordinate;
			int y_coordinate;
			do
			{
				cout << "Player " << pGame->GetPlayerTurn() << ", select a coordinate position (top left: 0,0; bottom right: 2,2).\nEnter each coordinate individually:\n";
				cin >> x_coordinate;
				cin >> y_coordinate;
			} while (!pGame->IsValidMove(x_coordinate, y_coordinate));

			pGame->Move(x_coordinate, y_coordinate);
		}

		// game over
		pGame->DisplayBoard();
		pGame->DisplayResult();


		// prompt to play again (or quit)
		char input = ' ';
		while (input != 'Y' && input != 'y')
		{
			std::cout << "Would you like to play again? (y/n): ";
			cin >> input;

			if (input == 'N' || input == 'n') return 0; // quit
		}
	}
}
