#pragma once
class TicTacToe
{
private:
	char m_board[3][3] = {
		{'_', '_', '_'},
		{'_', '_', '_'},
		{'_', '_', '_'}
	};
	int m_numTurns;
	int m_playerTurn;
	char m_winner;

	int m_row = 0;
	int m_column = 0;

public:
	TicTacToe();

	void DisplayBoard();
	int GetPlayerTurn();
	
	bool IsValidMove(int x_coordinate, int y_coordinate);
	void Move(int x_coordinate, int y_coordinate);

	bool IsOver();
	void DisplayResult();
};

