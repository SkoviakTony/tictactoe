#include "TicTacToe.h"

#include <iostream>

TicTacToe::TicTacToe()
{
	m_numTurns = 0;
	m_playerTurn = 1;
	m_winner = 0;
	m_row = 0;
	m_column = 0;
}

void TicTacToe::DisplayBoard()
{
	int row = 0;
	int column = 0;
	
	for(row; row <= 2; row++)
	{
		std::cout << "|";
		for(column = 0; column <= 2; column++)
		{
			std::cout << m_board[row][column] << "|";
		}
		column = 0;
		std::cout << "\n";
	}
}

void TicTacToe::DisplayResult()
{
	if (m_playerTurn == 3) std::cout << "Tie! No one wins.\n";
	else std::cout << "Player " << m_playerTurn << " wins!\n";
}

int TicTacToe::GetPlayerTurn()
{
	if(m_playerTurn == 1)
	{
		m_playerTurn = 2;
		return 1;
	}

	m_playerTurn = 1;
	return 2;
}

bool TicTacToe::IsOver()
{	
	if (m_board[0][0] == 'O' && m_board[0][1] == 'O' && m_board[0][2] == 'O'
		||
		m_board[1][0] == 'O' && m_board[1][1] == 'O' && m_board[1][2] == 'O'
		||
		m_board[2][0] == 'O' && m_board[2][1] == 'O' && m_board[2][2] == 'O'
		||
		m_board[0][0] == 'O' && m_board[1][0] == 'O' && m_board[2][0] == 'O'
		||
		m_board[0][1] == 'O' && m_board[1][1] == 'O' && m_board[2][1] == 'O'
		||
		m_board[0][2] == 'O' && m_board[1][2] == 'O' && m_board[2][2] == 'O'
		||
		m_board[0][0] == 'O' && m_board[1][1] == 'O' && m_board[2][2] == 'O'
		||
		m_board[0][2] == 'O' && m_board[1][1] == 'O' && m_board[2][0] == 'O'
		)
	{
		return true;
	}

	if (m_board[0][0] == 'X' && m_board[0][1] == 'X' && m_board[0][2] == 'X'
		||
		m_board[1][0] == 'X' && m_board[1][1] == 'X' && m_board[1][2] == 'X'
		||
		m_board[2][0] == 'X' && m_board[2][1] == 'X' && m_board[2][2] == 'X'
		||
		m_board[0][0] == 'X' && m_board[1][0] == 'X' && m_board[2][0] == 'X'
		||
		m_board[0][1] == 'X' && m_board[1][1] == 'X' && m_board[2][1] == 'X'
		||
		m_board[0][2] == 'X' && m_board[1][2] == 'X' && m_board[2][2] == 'X'
		||
		m_board[0][0] == 'X' && m_board[1][1] == 'X' && m_board[2][2] == 'X'
		||
		m_board[0][2] == 'X' && m_board[1][1] == 'X' && m_board[2][0] == 'X')
	{
		return true;
	}

	//Determines if the board is full. So long as it is not, returns false.
	for (m_row; m_row <= 2; m_row++)
	{
		for (m_column; m_column <= 2; m_column++)
		{
			if (m_board[m_row][m_column] == '_') return false;
		}
		m_column = 0;
	}
	
	//If neither person has won and the board is full, set player turn to 3 to report a tie in the results.
	m_playerTurn = 3;
	return true;
}

bool TicTacToe::IsValidMove(int x_coordinate, int y_coordinate)
{
	if (m_board[x_coordinate][y_coordinate] == '_') //MOVE CONDITION
	{
		return true;
	}
	GetPlayerTurn(); //Getting player turn again prompts the same player for a move until they select a valid square
	return false;
}

void TicTacToe::Move(int x_coordinate, int y_coordinate)
{
	if (m_playerTurn == 1) m_board[x_coordinate][y_coordinate] = 'O';
	if (m_playerTurn == 2) m_board[x_coordinate][y_coordinate] = 'X';
}
